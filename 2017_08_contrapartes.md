/*  Crear tabla como replica de la que vamos a cruzar  */
drop table estrategiasdb.AG_201708_Datos_duplicados_raw
create table estrategiasdb.AG_201708_Datos_duplicados_raw_2 stored as parquet as


/*  Crear tabla para comparar: Juntar las dos tablas en una sola y tener dos columnas con los nombres que queremos comparar  */
create table estrategiasdb.AG_201708_Datos_dup_comparar stored as parquet as

with y as (
with x as (
select a.* , b.fcmtcnremitente as fcmtcnremitente_2
from estrategiasdb.AG_201708_Datos_duplicados_raw as a
inner join estrategiasdb.AG_201708_Datos_duplicados_raw_2 as b
on a.nombre_benef_key = b.nombre_benef_key
)
select *,
      if (fcmtcnremitente = fcmtcnremitente_2, "yes","no") as cond
from x
where nombre_benef_key in("ROCIO CEBRERO SOBERANIS-1978/03/25")
)
select *
from y
where cond="no"
order by fcmtcnremitente,fcmtcnremitente_2





/*  Crear tabla para comparar: Juntar las dos tablas en una sola y tener dos columnas con los nombres que queremos comparar  */

create table estrategiasdb.AG_201708_Datos_dup_levenshtein stored as parquet as
select *,
levenshtein( fcmtcnremitente,fcmtcnremitente_2) as lev
from ag_201708_datos_dup_comparar







